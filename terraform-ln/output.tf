output "bucket_name" {
  description = "The name of the bucket"
  value       = aws_s3_bucket.website.id
}

output "bucket_endpoint" {
  description = "The endpoint of the bucket"
  value       = aws_s3_bucket.website.website_endpoint
}

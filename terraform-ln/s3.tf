resource "aws_s3_bucket" "website" {
  bucket = var.bucket_name
}

data "aws_s3_bucket" "ln_bucket" {
  bucket = aws_s3_bucket.website.bucket
}

# resource "aws_s3_bucket_acl" "ln_acl" {
#   bucket = data.aws_s3_bucket.ln_bucket.id
#   acl    = "public-read"
# }

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = data.aws_s3_bucket.ln_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "ln_bucket" {
  bucket = data.aws_s3_bucket.ln_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      # kms_master_key_id = aws_kms_key.bucket_census_publication.arn
      sse_algorithm = "AES256"
    }
  }
}



resource "aws_s3_bucket_policy" "ln-policy" {
  bucket = data.aws_s3_bucket.ln_bucket.id
  policy = data.aws_iam_policy_document.iam_policy_1.json
  depends_on = [ aws_s3_bucket_public_access_block.ln ]
}

data "aws_iam_policy_document" "iam_policy_1" {
  statement {
    sid    = "AllowPublicRead"
    effect = "Allow"

    actions = [
      "s3:*",
    ]

    resources = [
      "${aws_s3_bucket.website.arn}",
      "${aws_s3_bucket.website.arn}/*"
    ]

    principals {
      type        = "*"
      identifiers = ["*"]
    }
  }

}

resource "aws_s3_object" "object_upload_html" {
    for_each        = fileset("app/", "*.html")
    bucket          = data.aws_s3_bucket.ln_bucket.bucket
    key             = each.value
    source          = "app/${each.value}"
    content_type    = "text/html"
    etag            = filemd5("app/${each.value}")
}

resource "aws_s3_bucket_website_configuration" "website_config" {
  bucket = data.aws_s3_bucket.ln_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.jpeg"
  }

  routing_rule {
    condition {
      key_prefix_equals = "/ln"
    }
    redirect {
      replace_key_prefix_with = "comming-soon.jpeg"
    }
  }

}

resource "aws_s3_bucket_public_access_block" "ln" {
  bucket                  = data.aws_s3_bucket.ln_bucket.bucket
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

###################################################################################################
## Uncomment this block if we have a valid Domain to host/redirect via Route53.
###################################################################################################

# resource "aws_route53_zone" "main" {
#   name = var.domain_name
#   tags = {
#     Name = "www.${var.domain_name}"
#     description = var.domain_name
#   }
#   comment = var.domain_name
# }

# resource "aws_route53_record" "www_ln" {
#   zone_id = aws_route53_zone.main.zone_id
#   name = "www.${var.domain_name}"
#   type = "A"
#   alias {
#     name = data.aws_s3_bucket.ln_bucket.website_domain
#     zone_id = data.aws_s3_bucket.ln_bucket.hosted_zone_id
#     evaluate_target_health = false
#   }
# }

# resource "aws_s3_bucket_cors_configuration" "ln" {
#   bucket = data.aws_s3_bucket.ln_bucket.bucket
#   cors_rule {
#       allowed_headers = ["Authorization", "Content-Length"]
#       allowed_methods = ["GET", "POST"]
#       allowed_origins = ["https://www.${var.domain_name}"]
#       max_age_seconds = 3000
#     }
# }


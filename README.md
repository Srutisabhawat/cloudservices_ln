# CI/CD Pipeline With AWS Terraform Code

![Infrastructure Diagram](https://labresources.whizlabs.com/a1bb9f4fa305e97136c14d2013138f13/s3.png)

## Overview

This repository demonstrates the use of CI/CD tooling with GitLab and AWS infrastructure provisioning via Terraform. The primary infrastructure involves AWS S3 bucket resources, designed for hosting a static website or storing data.

## Goal

The objective is to design and implement a CI/CD pipeline that builds, tests, and deploys a single-page web application. The infrastructure necessary for these operations is defined as code using Terraform and is automatically handled through GitLab CI/CD stages.

## GitLab CI/CD Pipeline

The `.gitlab-ci.yml` file in this repository defines a CI/CD pipeline with the following stages:

1. **Build**: This is a placeholder stage with an echo command indicating the completion of the build stage.

2. **Test**: This is another placeholder stage with an echo command indicating the completion of the test stage. Here, you would typically include commands to test your code.

3. **Deploy**: This stage installs Terraform, initializes it, validates the Terraform code, creates a plan, and applies it to provision the AWS resources defined in the Terraform script.

4. **Teardown**: This stage is set to run manually. When triggered, it destroys the infrastructure created by Terraform. Be careful with this stage as it removes all the infrastructure set up by the Terraform script.

## Terraform Script

The Terraform code provisions the following AWS resources:

1. **S3 Bucket (`aws_s3_bucket`)**: Creates an S3 bucket where the name is provided through a variable `bucket_name`.

2. **S3 Bucket Versioning (`aws_s3_bucket_versioning`)**: Enables versioning for the S3 bucket, allowing it to keep multiple versions of an object.

3. **S3 Bucket Server-side Encryption (`aws_s3_bucket_server_side_encryption_configuration`)**: Enables server-side encryption for the S3 bucket using the AES256 algorithm.

4. **S3 Bucket Policy (`aws_s3_bucket_policy`)**: Defines an S3 bucket policy that allows public read access to the bucket's content.

5. **S3 Objects (`aws_s3_object`)**: Uploads HTML files from the `app/` directory of the project to the S3 bucket. It identifies all `.html` files in the `app/` directory and uploads each as an S3 object.

6. **S3 Bucket Website Configuration (`aws_s3_bucket_website_configuration`)**: Defines the website configuration for the S3 bucket, setting up `index.html` as the index document and `404.jpeg` as the error document.

7. **S3 Public Access Block (`aws_s3_bucket_public_access_block`)**: Configures public access settings for the bucket, currently allowing all types of public access.

The script also includes commented-out sections that provide additional features:

- Route53 records and zones (`aws_route53_zone` and `aws_route53_record`): Used for setting up a custom domain for the website hosted on the S3 bucket. 
- CORS configuration (`aws_s3_bucket_cors_configuration`): Can be used to allow cross-origin requests to the bucket from specified origins.

## Deployment

After deploying the Terraform code using the CI/CD pipeline, you can view the hosted static page at this URL: [Website](https://lexus-tp-384he2.s3.eu-west-2.amazonaws.com/index.html)

## Note

This repository does not include setup for Terraform remote state and Terraform DynamoDB lock. If you plan to create a remote state bucket and dynamic Terraform lock, you need to retrofit this repository accordingly.
